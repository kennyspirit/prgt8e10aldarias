/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Ejemplo0805a.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0805a {

  public static String leercadena() {
    String cad = "";
    BufferedReader bufferedReader =
            new BufferedReader(new InputStreamReader(System.in));
    try {
      cad = bufferedReader.readLine();
    } catch (IOException e) {
    }
    return cad;
  }

  public static void main(String args[]) {
    String cad;
    System.out.println("Hace eco hasta que escribas para");
    do {
      cad = leercadena();
      System.out.println(cad);
    } while (!cad.equals("para"));
  }
}

/* EJECUCION
 Hace eco hasta que escribas para
 abc
 abc
 para
 para
 */
