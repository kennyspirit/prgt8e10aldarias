/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Fichero: Ejemplo0812.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
public class Ejemplo0812 {

  public static void main(String args[]) {
    FileInputStream f = null;
    String s = "";
    char c;
    try {
      f = new FileInputStream("Ejemplo0812.bin"); // bytes
      int size = f.available();
      for (int i = 0; i < size; i++) {
        c = (char) f.read(); // Lee 1 byte, convierte a char
        s = s + c;
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      System.out.println(s);
      try {
        f.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
/* EJECUCION:
 Texto
 */
