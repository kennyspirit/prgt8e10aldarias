/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt8e10aldarias;

import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Fichero: Ejemplo0814Html.java
 * @date 30-ene-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejemplo0814Html {

  public static void main (String args[]) {
  String cad = "Ejemplo0814Html";
       
 FileWriter filewriter = null;
 PrintWriter printw = null;
       
 try{
     filewriter = new FileWriter("Ejemplo0807Html.html");//declarar el archivo
     printw = new PrintWriter(filewriter);//declarar un impresor
           
     printw.println("<html>");
     printw.println("<head><title>Ejemplo0814Html</title></head>");    
     //si queremos escribir una comilla " en el
     //archivo uzamos la diagonal invertida \"
     printw.println("<body bgcolor=\"#99CC99\">");
    
     //si quisieramos escribir una cadena que vide de una lista o
     //de una variable lo concatenamos
     printw.println("<center><h1><font color=\"navy\">"+cad+"</font></h1></center>");
     printw.println("<center><h4><font color=\"purple\">Ejemplo de FileWriter</font></h4></center>");
    
 
     printw.println("</body>");
     printw.println("</html>");
           
     //no devemos olvidar cerrar el archivo para que su lectura sea correcta
     printw.close();//cerramos el archivo
 }catch (Exception e){}      
     System.out.println("Generada página web");//si todo sale bien mostramos un mensaje de guardado exitoso
  }
}
/* EJECUCION
Generada página web
*/
