/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Ejemplo0803.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0803 {

  public static void main(String args[]) throws IOException {
    String tmp = "abcdefghijklmnopqrstuvwxyz";
    int length = tmp.length();
    char c[] = new char[length];
    tmp.getChars(0, length, c, 0);
    CharArrayReader input1 = new CharArrayReader(c);
    CharArrayReader input2 = new CharArrayReader(c, 0, 5);
    int i;
    System.out.println("input1 is:");
    while ((i = input1.read()) != -1) {
      System.out.print((char) i);
    }
    System.out.println();
    System.out.println("input2 is:");
    while ((i = input2.read()) != -1) {
      System.out.print((char) i);
    }
    System.out.println();
  }
}

/* EJECUCION
input1 is:
abcdefghijklmnopqrstuvwxyz
input2 is:
abcde 
 */
