/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Ejemplo0805b.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0805b {

  public String cad = "";

// Devuelve cadena leida por teclado
  public void leercadena() {
    BufferedReader bufferedReader;
    bufferedReader =
            new BufferedReader(new InputStreamReader(System.in));
    try {
      cad = bufferedReader.readLine();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
// Pide lectura cadenas

  public void leercadenas() {
    System.out.println("Hace eco hasta que escribas para");
    do {
      leercadena();
      System.out.println(cad);
    } while (!cad.equals("para"));
  }

// Main
  public static void main(String args[]) {
    new Ejemplo0805b().leercadenas();
  }
}

/* EJECUCION
 Hace eco hasta que escribas para
 abc
 abc
 para
 para
 */
