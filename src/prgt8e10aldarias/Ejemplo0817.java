/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Fichero: Ejemplo0817.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
public class Ejemplo0817 {

  public static void main(String[] args) {
    BufferedReader br = null;
    try {
      String sCurrentLine;
      br = new BufferedReader(new FileReader("bufferedreader02.txt"));
      while ((sCurrentLine = br.readLine()) != null) {
        System.out.println(sCurrentLine);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        if (br != null) {
          br.close();
        }
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  } // main
} // class

/* EJECUCION:
Esto es
una prueba
*/
