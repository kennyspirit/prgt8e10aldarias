/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Fichero: Ejemplo0816.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
// Main
public class Ejemplo0816 {

  public static void main(String[] arguments) {
    TodoMayusculas may = new TodoMayusculas("Ejemplo0816.txt");
    may.convertir();
  }
}
// Clase
class TodoMayusculas {

  String nombreFuente;

  TodoMayusculas(String fuenteArg) {
    nombreFuente = fuenteArg;
  }

  void convertir() {
    try {
      // Crear objetos file
      File fuente = new File(nombreFuente);
      File temp = new File("may" + nombreFuente + ".tmp");
      // Crear stream input
      FileReader fr = new FileReader(fuente);
      BufferedReader in = new BufferedReader(fr);
      // Crear stream output
      FileWriter fw = new FileWriter(temp);
      BufferedWriter out = new BufferedWriter(fw);
      boolean eof = false;
      int inChar = 0;
      do {
        inChar = in.read();
        System.out.print((char) inChar);
        System.out.println();
        if (inChar != -1) {
          char outChar = Character.toUpperCase((char) inChar);
          out.write(outChar);
          System.out.print(outChar);
          System.out.println();
        } else {
          eof = true;
        }
      } while (!eof);
      in.close();
      out.close();
      boolean borrado = fuente.delete();
      if (borrado) {
        temp.renameTo(fuente);
      }
    } catch (IOException e) {
    } catch (SecurityException se) {
    }
  }
}
